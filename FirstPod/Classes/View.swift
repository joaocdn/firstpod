//
//  View.swift
//  Pods
//
//  Created by Joao Nascimento on 31/08/16.
//
//

import Foundation

public extension UIView {
    
    // ***** VIEW SIZE *****
    
    var size : CGSize {
        get {
            return frame.size
        }
        set {
            frame = CGRectMake(left, top, newValue.width, newValue.height)
        }
    }
    
    var width : CGFloat {
        get {
            return frame.height
        }
        set {
            frame = CGRectMake(left, top, newValue, height)
        }
    }
    
    var height : CGFloat {
        get {
            return frame.height
        }
        set {
            frame = CGRectMake(left, top, width, newValue)
        }
    }
    
    // ***** VIEW POSITION *****
    
    var left : CGFloat {
        get {
            return frame.origin.x
        }
        set {
            frame.origin.x = newValue
        }
    }
    
    var top : CGFloat {
        get {
            return frame.origin.y
        }
        set {
            frame.origin.y = newValue
        }
    }
    
    var right : CGFloat {
        get {
            return left + width
        }
        set {
            left = newValue - width
        }
    }
    
    var bottom : CGFloat {
        get {
            return top + height
        }
        set {
            top = newValue - height
        }
    }
    
    var centerX : CGFloat {
        get {
            return left + width/2
        }
        set {
            left = newValue - width/2
        }
    }
    
    var centerY : CGFloat {
        get {
            return top + height/2
        }
        set {
            top = newValue - height/2
        }
    }
    
    // ***** VIEW ALIGNEMENT *****
    
    func centerInView(view: UIView) {
        self.centerX = view.width/2
        self.centerY = view.height/2
    }
    
    func centerAlignX(view: UIView) {
        self.centerX = view.width/2
    }
    
    func centerAlignY(view: UIView) {
        self.centerY = view.height/2
    }
    
    func sameSizeAsView(view: UIView) {
        self.size = view.size
    }
}
