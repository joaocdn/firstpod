//
//  String.swift
//  Pods
//
//  Created by Joao Nascimento on 31/08/16.
//
//

import Foundation

public extension String{
    func empty() -> Bool {
        return self.characters.count == 0
    }
}