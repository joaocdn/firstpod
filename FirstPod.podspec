#
# Be sure to run `pod lib lint FirstPod.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'FirstPod'
  s.version          = '1.0.1'
  s.summary          = 'This is a short description of my first pod. Trying to make it work!'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = 'This is a short description of my first pod. Trying to make it work! It has some extensions and it should work fine :)'

  s.homepage         = 'https://bitbucket.org/joaocdn/firstpod'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Joao Nascimento' => 'joao.nascimento@tamtam.nl' }
  s.source           = { :git => "https://joaocdn@bitbucket.org/joaocdn/firstpod.git", :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.source_files = 'FirstPod/Classes/**/*'
  
  # s.resource_bundles = {
  #   'FirstPod' => ['FirstPod/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
